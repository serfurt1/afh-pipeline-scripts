### Extracts the lemmas from the wiktionary data csv: (inflected_form),(lemma),(features)
### Needed as part of the inputs to run AhlBerg software

import codecs
import sys
import os

if len(sys.argv) < 2:
    print 'Usage: extract-baseforms [path_to_inflected_forms_csv] [-d] [path_to_inflected_forms_dir]'
    sys.exit()
option = sys.argv[1]
if(os.path.isfile(option)):
    filename = option
    dirname,basename = os.path.split(filename)
    if not dirname:
        dirname = '.'
    langname = '_'.join(os.path.basename(basename).split('.')[0].split('_')[-2:])
    baseformsfile = os.path.normpath('%s/base_forms_%s.txt' % (dirname,langname))
    baseforms = []
    current = None
    for line in codecs.open(filename,'r',encoding='utf-8'):
        if line.strip():
            base = line.strip().split(',')[1]
            if not current:
                current = base
                continue
            if base != current:
                baseforms.append(current)
                current = base
    codecs.open(baseformsfile, 'w', encoding='utf-8').write('\n'.join(baseforms))
# Sean: modified for handling whole directory
elif(option == '-d'):
    inDir = sys.argv[2]
    # append to existing Makefile.data from previous script
    data = open("Makefile.data","a")
    data.write("\n# Base Forms\n")
    for f in os.listdir(inDir):
        if not (f.startswith("Makefile")):
    	    filename = inDir + f
            pos = f.split('.')[0].rsplit('_')[-1].encode("UTF-8")
            dirname,basename = os.path.split(filename)
            if not dirname:
                dirname = '.'
    	    # languages can come in parts, get all except inflections_input_ and _<pos>.csv
            langname = '_'.join(basename.split('_')[2:-1])
    	    baseformsfile = os.path.normpath('%s/base_forms_%s_%s.txt' % (dirname,langname, pos))
            # generate line for Makefile.data
    	    data.write(langname+"-"+pos.upper()+"-XVAL="+baseformsfile+"\n")
            baseforms = []
            current = None
            for line in codecs.open(filename,'r',encoding='utf-8'):
                if line.strip():
                    try:
                        base = line.strip().split(',')[1]
                    except IndexError:
                        base = line.strip()
                    if not current:
                        current = base
                        continue
                    if base != current:
                        baseforms.append(current)
                        current = base
            codecs.open(baseformsfile, 'w', encoding='utf-8').write('\n'.join(baseforms))
    data.close()

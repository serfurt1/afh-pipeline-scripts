#!/bin/bash

usage ()
{
  echo 'Usage : run_per_language [-i] -l <language_pos> -d <inflections data directory>'
  echo 'Options: -i -- to run individual inflection feature vector models'
  exit
}

lang=
indiv=
dir=
while [ "$1" != "" ]; do
    case $1 in
        -i | --individual )    indiv=1
								;;
        -l | --lang )           shift 
                                lang=$1
                                ;;
        -d | --data )           shift 
                                dir=$1
                                ;;
        * )                     usage
    esac
    shift
done

if [ ! $lang ] || [ ! $dir ]; then
	usage
fi

mkdir -p stdout
if [ -z $indiv ] ; then
	#echo $dir
	export LANGVAR="data/wikipedia/stats_en.txt ${dir}/${lang}.csv ${dir}/${lang}_train.txt"
	export DIR_VAR=''
	make "output/${lang}.txt" > "stdout/${lang}"
	# predict
	python src/predict.py -test "${dir}/${lang}_test.txt" -infl "${dir}/${lang}.csv" -xval "data/xval/index.txt" \
		-config classifier_svm.conf -C classifier_svm classifier_maxsuff -l $lang -o "predictions/" > "stdout/${lang}"
else
	for subdir in $(find ${dir} -mindepth 1 -maxdepth 1 -type d | awk -F/ '{print $NF}') 
	do
		if [ ! -e "${dir}/${subdir}/${lang}.csv" ]
		then
			continue
		fi
		export LANGVAR="data/wikipedia/stats_en.txt ${dir}/${subdir}/${lang}.csv ${dir}/${subdir}/${lang}_train.txt"
		export DIR_VAR="${subdir}/"
		#echo "${dir}/${subdir}/${lang}.csv"
		mkdir -p "stdout/${subdir}/"
		make "output/${subdir}/${lang}.txt"	> "stdout/${subdir}/${lang}"
		# predict
		mkdir -p "predictions/${subdir}/"
		python src/predict.py -test "${dir}/${subdir}/${lang}_test.txt" -infl "${dir}/${subdir}/${lang}.csv" \
			-xval "data/xval/${subdir}/index.txt" -config classifier_svm.conf -C classifier_svm classifier_maxsuff \
			-l $lang -o "predictions/${subdir}/" > "stdout/${subdir}/${lang}"
	done
fi

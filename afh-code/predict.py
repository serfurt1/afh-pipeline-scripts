# -*- encoding: UTF-8 -*-
from __future__ import division
import codecs
import corpus as C
import numpy as np
import os.path
from paradigms import Paradigms 
import random
import sys
from collections import defaultdict


# TODO ska vi ha med korpus-koden?
"""
This module is resposible for the cross evaluation.
The data should be prepared (by using preprocess.py) and
the paths to the needed files for each iteration should be 
found in an index file (data/xval/index.txt).
"""

def eval(classifiers, language, test_file, inflections_file, out, xval='', path="../", verbose=True, config='classifier_svm.conf', langdir={}):

    """
       Evaluates the specified classifiers on the specified languages
    """
    if xval:
        index_file = read_index(xval)
    else:
        index_file = langdir
    fold_data = index_file[language]
    # the corpus file should always be in data/wikipedia
    corpus_file = path+'data/wikipedia/stats_'+language.split('_')[0]+'.txt' 
    if verbose:   print 'Predicting with Cross validation on %s with %d folds' % (language, len(fold_data))
    predictions = []
    corpus = C.Corpus(corpus_file)
    paras = [Paradigms(info[0]) for  info in fold_data]
    featureVecs = get_feature_vectors(inflections_file)
    words = codecs.open(test_file,encoding='utf-8').readlines()
    for cname, c in classifiers:
        fold_predictions = [[{} for fv in featureVecs] for w in words]
        # go through the folds
        for i, (para, _, _) in enumerate(fold_data):
            p = paras[i]
            if verbose==2: 
                print 'Iteration',i,
                print 'paradigms',p.paradigmfilename, len(words)
            if "svm" in cname: # all svms want a config?
                c.config = path+config
                if verbose==2: print 'configuration file'':',c.config
                c.thislang = os.path.normpath(para)
                c.out = out
            c.train(p, corpus)
            pred = predict(c, words, p, verbose=verbose)
            for j,(word_predictions, confidence) in enumerate(pred):
                for k,inflected_form_pred in enumerate(word_predictions):
                  try:
                    if inflected_form_pred in fold_predictions[j][k].keys():
                        (num, conf) = fold_predictions[j][k][inflected_form_pred]
                        fold_predictions[j][k][inflected_form_pred] = (num+1, conf+confidence)
                    else:
                        fold_predictions[j][k][inflected_form_pred] = (1, confidence)
                  except Exception, e:
                    print e
                    print 'Features: ', len(featureVecs), featureVecs
                    print 'Predictions: ', len(word_predictions), word_predictions
                    raise e

        for j, fold_inflections in enumerate(fold_predictions):
            for k, infl_form_pred_dct in enumerate(fold_inflections):
                if not infl_form_pred_dct.keys():
                  continue
                inflected_form_pred = max(infl_form_pred_dct.keys(), key=lambda p: infl_form_pred_dct[p])
                (num, conf) = infl_form_pred_dct[inflected_form_pred]
                predictions.append('%s,%s,%s,%s,%.2f' %(words[j].strip(), featureVecs[k], inflected_form_pred, cname, conf/num))
    
    print 'Writing predictions to %s/predictions_%s.txt' %(output, language)
    out = codecs.open('%s/predictions_%s.txt' %(output, language),'w',encoding='utf-8').write('\n'.join(predictions))

def get_feature_vectors(inflections_file):
    firstLemma = ''
    featureVecs = []
    for line in codecs.open(inflections_file, 'r', encoding='utf-8'):
        line = line.strip()
        [_,lemma,feat] = line.split(',')
        if not firstLemma:
            firstLemma = lemma
        if lemma == firstLemma:
            featureVecs.append(feat)
        else:
            break
    return featureVecs

def read_index(indexfile):
  # lang : (para, train, test, tables, all_bases) 
  index_dir = {}
  for line in codecs.open(indexfile).readlines():
      if line.strip():
          xs = line.strip().split('\t')
          index_dir.setdefault(xs[0],[]).append((path+xs[1],path+xs[3],path+xs[4]))
  return index_dir


def find_conf(langname, p, c, t, test,config):
    if os.path.isfile(config):
        return config
    else:
        return 'src/'+config

def predict(classifier, words, paradigmobj, verbose=False):
    """
       Predict the inflection tables for the test words
    """
    predictions = []
    for word in words:
        word = word.strip()
        bs = classifier.classify(word)
        if not bs:
            print 'No output from classifier for word ', word
            predictions.append(([],0.0))
            continue
        b, confidence = bs[0]
        table = paradigmobj.get_table(b, word)
        if table:
            if verbose==2:
                print >> out, '\t%d/%d: %s as %s (%.2f)?'% (i,len(bs),word,b,confidence)
        predictions.append((table[0], confidence))
    return predictions

def set_path():
    # If file is run from Make (..) set path to other files to '.',
    # if run from src, set path to other files to '..'
    if os.path.dirname(sys.argv[0]):
        return "./"
    else:
        return "../"


all_languages =  ["de_noun","de_verb","es_verb","fi_nounadj","fi_verb","mt_verbs","ca_nouns","ca_verbs","en_nouns","en_verbs","fr_nouns","fr_verbs","gl_nouns","gl_verbs","it_nouns","it_verbs","pt_nouns","pt_verbs","ru_nouns","ru_verbs"]


usage = """
Usage: python evaluate.py -c classifier1 classifier2 ... classifierN -l en_nouns de_noun -xval indexfile.txt 
       or for all languages, all classifiers:
       python evaluate.py -ca ... classifierN -la  -xval indexfile.txt  
        -v[0,1,2]   verbose 
        -h   help
"""

if __name__ == "__main__":
    
    inputs  = False
    verbose = True
    find_output, output = False, 'std_output.txt'
    find_xval, xval_file = False, ''
    find_config, config = False, ''
    use_all_classifiers = False
    classnames, classifiers, language = [], [], ''
    path = set_path()
    test, test_file = False, ''
    infl, infl_file = False, ''

    # non-elegant argument parsing
    if "-h" in sys.argv:
       print usage
       sys.exit()

    for arg in sys.argv[1:]:
       if arg == "-v0":
           verbose = False
       if arg == "-v2":
           verbose = 2
       elif arg == "-xval":
           find_xval = True
       elif arg == "-config":
           find_config = True
       elif arg == "-ca":
           use_all_classifiers = True
       elif arg == "-la":
           use_all_languages = True
       elif arg == "-o":
           find_output = True
           inputs = False
       elif arg == "-l":
           inputs = True
       elif arg == "-test":
           test = True
       elif arg == "-infl":
           infl = True
       elif find_output:
           output = arg
           find_output = False
       elif test:
           test_file = arg
           test = False
       elif infl:
           infl_file = arg
           infl = False
       elif find_config:
           config = arg
           find_config = False
       elif find_xval:
           xval_file = arg
           find_xval = False
       elif inputs:
           language = arg
       else:
           classnames.append(arg)

    if use_all_classifiers:
        classnames = all_classifiers
    # load the given classifiers
    for c in classnames:
        if c == "classifier_svm":
            import classifier_svm
            classifiers.append((c,classifier_svm.Classifier()))        
        elif c == "classifier_maxsuff":
            import classifier_maxsuff
            classifiers.append((c,classifier_maxsuff.Classifier()))
    if not classifiers:
        print >> sys.stderr, 'No classifiers given'
        sys.exit(1)
    if not test_file:
        print >> sys.stderr, 'No test file given'
        sys.exit(1)
    if not infl_file:
        print >> sys.stderr, 'No inflections file given'
        sys.exit(1)
    #with  codecs.open(output,'w',encoding='utf-8') as out:
    eval(classifiers, language, test_file, infl_file, output, verbose=verbose, path=path, config=config, xval=xval_file)


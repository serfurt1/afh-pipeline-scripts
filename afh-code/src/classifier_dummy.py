import random

class Classifier:

    def __init__(self):
        self.paradigms = None
        self.corpus    = None

    def train(self, paradigms, corpus):
        self.paradigms = paradigms
        self.corpus    = corpus

    def classify(self, word):
        xs = self.paradigms.paradigms.keys()
        random.shuffle(xs)
        p = 1.0/len(xs)
        return [(x,p) for x in xs]

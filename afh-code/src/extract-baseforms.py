### Extracts the lemmas from the wiktionary data csv: (inflected_form),(lemma),(features)
### Needed as part of the inputs to run AhlBerg software
###EDIT: Also rewrites the inflections csv file in the proper order
###      (So the entire table for each lemma appear together)

import codecs
import sys
import os
from collections import defaultdict

if len(sys.argv) < 2:
    print 'Usage: extract-baseforms [path_to_inflected_forms_csv]'
    sys.exit()
filename = sys.argv[1]
if(os.path.isfile(filename)):
    dirname,basename = os.path.split(filename)
    if not dirname:
        dirname = '.'
    langname = '_'.join(os.path.basename(basename).split('.')[0].split('_')[-2:])
    baseformsfile = os.path.normpath('%s/base_forms_%s.txt' % (dirname,langname))
    #baseforms = []
    #current = None
    # I'm using a dictionary because the lines don't necessarily come on order
    # of the lemmas.
    dct = defaultdict(list)
    for line in codecs.open(filename,'r',encoding='utf-8'):
        line = line.strip()
        if line:
            base = line.split(',')[1]
            dct[base].append(line)
    codecs.open(filename, 'w', encoding='utf-8').write('\n'.join([line for lemma_lines in dct.values() for line in lemma_lines]))
    codecs.open(baseformsfile, 'w', encoding='utf-8').write('\n'.join(dct.keys()))
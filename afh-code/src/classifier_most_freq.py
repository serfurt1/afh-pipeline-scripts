import codecs
import math
import corpus
import paradigms
from classifier_maxsuff_c import Classifier as CClassifier

class Classifier(CClassifier):

    def __init__(self):
        CClassifier.__init__(self)

    def train(self, paradigms, corpus_in):
        self.paradigms = paradigms
        self.corpus    = corpus.Corpus()

    def classify(self, word):
        return super(Classifier,self).classify(word, [True,False,False])

    
if __name__ == "__main__":
    cl = Classifier()
    P = paradigms.Paradigms('../paradigms/de_nouns_train.para')
    C = corpus.Corpus(None)
    cl.train(P,C)
    print "\n".join(['%s %.2f'%(w,d) for (w,d) in cl.classify('Buch')])

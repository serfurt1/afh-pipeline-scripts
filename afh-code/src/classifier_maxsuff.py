import codecs
import math
import corpus
import paradigms
from classifier_maxsuff_c import Classifier as CClassifier


class Classifier(CClassifier):

    def __init__(self):
        CClassifier.__init__(self)

    def train(self, paradigms, corpus_in):
        self.paradigms = paradigms
        self.corpus    = corpus.Corpus()


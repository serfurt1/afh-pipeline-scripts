#!/bin/bash

usage ()
{
  echo 'Usage : run_per_language -train <Durrett_DeNero train/test directory> -infl <inflections_data_directory> -data <directory to store copied train files and inflections data> -max_para_dir <directory containing Durrett/DeNero stdout files containing max paradigms>'
  exit
}

train=
infl=
output=
para_dir
while [ "$1" != "" ]; do
    case $1 in
        -train )	shift 
           			train=$1
                	;;
        -data )		shift 
           			output=$1
                	;;
        -infl )		shift 
           			infl=$1
                	;;
        -max_para_dir )		shift 
           					para_dir=$1
                			;;


		* )		usage
    esac
    shift
done

if [ ! $train ] || [ ! $infl ] || [ ! $output ] || [ ! $para_dir ]; 
then
	usage
fi

python extract-max-paradigm-and-rewrite-inflections-data.py $para_dir $infl $output

for train_test in $( ls ${train} | egrep *[train\|test].txt )
do
	cp "${train}/${train_test}" $output
done



import pandas as pd
import os
import sys
import codecs
from collections import defaultdict

"""
Takes in the inflection tables and generates new inflection table files for each individual inflection feature vector. 
Assumes that the data needed to run the max-paradigm AFH & Durrett_DeNero models have been set up.
For both models, it creates a directory in the main data directory for each inflection feature vector and saves the new files(inflection tables, train and test files)
for each language in the newly-created directory.
"""

def extract_indiv_lemma_train_set(inflectionDataDir, trainTestOutputDirDD, trainTestOutputDirAFH):

    inflectionDataFileList = os.listdir(inflectionDataDir)
    
    for infl_data_file in inflectionDataFileList:
        language_pos = infl_data_file.rsplit('.', 1)[0]
        print 'Language:', language_pos
        inflectionData = pd.read_csv(inflectionDataDir+infl_data_file,header=None,dtype=unicode,encoding="utf-8",names=["infl","lemma","featureVector"])
        test_file = '%s%s_test.txt' %(trainTestOutputDirDD, language_pos)
        if not os.path.exists(test_file):
            print 'Skipping %s - No %s test file' %(infl_data_file, test_file)
            continue
        # get test lemmas from Durrett and DeNero(or max paradigm AHF) run, same test lemmas used so as to combine models
        test_file_obj = codecs.open(test_file, 'r', encoding='utf-8')
        test_lemmas = set()
        for line in test_file_obj.readlines():
            test_lemmas.add(line.strip())
        # for each unique feature vector, get the lemmas
        for featureVec in set(inflectionData['featureVector']):
            featureVecLemmas = inflectionData[['lemma', 'infl']][inflectionData['featureVector']==featureVec]
            featureVectorTrainLemmas = set(featureVecLemmas['lemma']) - test_lemmas
            # 50 - If not up to 50 lemmas for training a model for this indiv inflection feature vector,
            # then skip
            if len(featureVectorTrainLemmas) < 50:
                continue
            #the name of the new directory is (basically) the feature vector
            newFeatureVecDir = featureVec.replace('=', '-').replace(':', '--').replace('/', '_') + '/'
            #form the full path names for both the DD & AFH train/test directories
            #and create the directory if it doesn't exist
            newFeatureVecDirFullPathDD = trainTestOutputDirDD + newFeatureVecDir 
            if not os.path.isdir(newFeatureVecDirFullPathDD):
                os.mkdir(newFeatureVecDirFullPathDD)
            newFeatureVecDirFullPathAFH = trainTestOutputDirAFH + newFeatureVecDir 
            if not os.path.isdir(newFeatureVecDirFullPathAFH):
                os.mkdir(newFeatureVecDirFullPathAFH)
            # Some lemmas have more than one inflected form for a particular feature vec.
            # Save as 'form1|form2, lemma, feature_vec'
            # For Durrett/DeNero: 'form1=====form2, lemma, feature_vec'
            # I think DD ends up using whichever form comes first, so might not actually matter
            featureVecLemmasDict = defaultdict(str)
            for i in range(len(featureVecLemmas)):
                lemma = featureVecLemmas['lemma'].iloc[i]
                inflected_form = featureVecLemmas['infl'].iloc[i]
                previous_forms = featureVecLemmasDict[lemma]
                if not previous_forms:
                    featureVecLemmasDict[lemma] = inflected_form
                else:
                    featureVecLemmasDict[lemma] = '%s|%s' %(previous_forms, inflected_form)
            # the lines to be written into the new train file for this infl feature vector
            new_inflections_linesDD = []
            new_inflections_linesAFH = []
            for lemma, inflected_form in featureVecLemmasDict.items():
                # AFH
                new_inflections_linesAFH.append('%s,%s,%s' %(lemma, lemma, 'Input'))
                new_inflections_linesAFH.append('%s,%s,%s' %(inflected_form, lemma, featureVec))
                # DD
                new_inflections_linesDD.append('%s,%s,%s' %(inflected_form.replace('|', '====='), lemma, featureVec))
            """for i in range(len(featureVecLemmas)):
                lemma = featureVecLemmas['lemma'].iloc[i]
                inflected_form = featureVecLemmas['infl'].iloc[i]
                new_inflections_lines.append('%s,%s,%s' %(lemma, lemma, 'Input'))
                new_inflections_lines.append('%s,%s,%s' %(inflected_form, lemma, featureVec))"""
            new_inflections_linesStrAFH = '\n'.join(new_inflections_linesAFH)
            new_inflections_linesStrDD = '\n'.join(new_inflections_linesDD)

            featureVectorTrainLemmasStr = '\n'.join(featureVectorTrainLemmas)
            test_lemmasStr = '\n'.join(test_lemmas)

            # Write the different files
            # AFH
            codecs.open('%s%s.csv' %(newFeatureVecDirFullPathAFH, language_pos), 'w', encoding='utf-8').write(new_inflections_linesStrAFH)
            codecs.open('%s%s_train.txt' %(newFeatureVecDirFullPathAFH, language_pos), 'w', encoding='utf-8').write(featureVectorTrainLemmasStr)
            codecs.open('%s%s_test.txt' %(newFeatureVecDirFullPathAFH, language_pos), 'w', encoding='utf-8').write(test_lemmasStr)

            # DD
            codecs.open('%s%s.csv' %(newFeatureVecDirFullPathDD, language_pos), 'w', encoding='utf-8').write(new_inflections_linesStrDD)
            codecs.open('%s%s_train.txt' %(newFeatureVecDirFullPathDD, language_pos), 'w', encoding='utf-8').write(featureVectorTrainLemmasStr)
            codecs.open('%s%s_test.txt' %(newFeatureVecDirFullPathDD, language_pos), 'w', encoding='utf-8').write(test_lemmasStr)

if len(sys.argv) != 4:
    print 'Usage: extract-indiv-paradigm-build-train-test.py [inflections-data-dir directory-containing-the-data-for-the-max-paradigm-model] \
        [DD-train-test-dir directory containing the train/test data for the Durrett_DeNero model] \
        [AFH-train-test-dir directory containing the train/test data for the AFH model]'
    sys.exit()

inflectionDataDir = sys.argv[1]
dd_trainTestOutputDir  = sys.argv[2]
afh_trainTestOutputDir = sys.argv[3]

extract_indiv_lemma_train_set(inflectionDataDir, dd_trainTestOutputDir, afh_trainTestOutputDir)


# PROGRAMS
P=python src/paradigms.py
PP=python printwordforms.py
E=./extract.perl -u
XEVAL=python src/evaluate.py
PREPROCESS=python src/preprocess.py
SEED=41
FOLDS=5
NO_BASEFORMS=5000
CLASSIFIERS=classifier_dummy classifier_most_freq classifier_maxsuff classifier_svm


CONFFILE=classifier_svm.conf
INDEXDIR=data/xval/
INDEXFILE=${INDEXDIR}index.txt
OUTPUT=output/
EVAL-RES=${OUTPUT}/eval.txt
TMP=tmp/

include Makefile.data

############ FILE LISTS FOR CROSS VALIDIATION #############
# All languages should be declared here
LANGS=de_noun de_verb es_verb fi_nounadj fi_verb mt_verbs ca_nouns ca_verbs en_verbs fr_nouns fr_verbs gl_nouns gl_verbs it_nouns it_verbs pt_nouns pt_verbs ru_nouns ru_verbs

de_noun= ${WIKIDE} ${DE-INFL-NOUN-XVAL} ${DE-NOUN-XVAL}
de_verb= ${WIKIDE} ${DE-INFL-VERB-XVAL} ${DE-VERB-XVAL}
es_verb= ${WIKIES} ${ES-INFL-VERB-XVAL} ${ES-VERB-XVAL}
fi_nounadj= ${WIKIFI} ${FI-INFL-NOUNADJ-XVAL} ${FI-NOUNADJ-XVAL}
fi_verb= ${WIKIFI} ${FI-INFL-VERB-XVAL} ${FI-VERB-XVAL}
ca_nouns= ${WIKICA} ${CA-INFL-NOUN} ${CA-NOUN-XVAL}
ca_verbs= ${WIKICA} ${CA-INFL-VERB} ${CA-VERB-XVAL}
en_nouns= ${WIKIEN} ${EN-INFL-NOUN} ${EN-NOUN-XVAL}
en_verbs= ${WIKIEN} ${EN-INFL-VERB} ${EN-VERB-XVAL}
fr_nouns= ${WIKIFR} ${FR-INFL-NOUN} ${FR-NOUN-XVAL}
fr_verbs= ${WIKIFR} ${FR-INFL-VERB} ${FR-VERB-XVAL}
gl_nouns= ${WIKIGL} ${GL-INFL-NOUN} ${GL-NOUN-XVAL}
gl_verbs= ${WIKIGL} ${GL-INFL-VERB} ${GL-VERB-XVAL}
it_nouns= ${WIKIIT} ${IT-INFL-NOUN} ${IT-NOUN-XVAL}
it_verbs= ${WIKIIT} ${IT-INFL-VERB} ${IT-VERB-XVAL}
pt_nouns= ${WIKIPT} ${PT-INFL-NOUN} ${PT-NOUN-XVAL}
pt_verbs= ${WIKIPT} ${PT-INFL-VERB} ${PT-VERB-XVAL}
ru_nouns= ${WIKIRU} ${RU-INFL-NOUN} ${RU-NOUN-XVAL}
ru_verbs= ${WIKIRU} ${RU-INFL-VERB} ${RU-VERB-XVAL}
mt_verbs= ${WIKIMT} ${MT-INFL-VERB} ${MT-VERB-XVAL}

# OUR LANGUAGES
qu_noun= ${WIKI} ${QU-INFL-NOUN-XVAL} ${QU-NOUN-XVAL}
qu_verb= ${WIKI} ${QU-INFL-VERB-XVAL} ${QU-VERB-XVAL}
hau_noun= ${WIKI} ${HAU-INFL-NOUN-XVAL} ${HAU-NOUN-XVAL}
ir_noun= ${WIKI} ${IR-INFL-NOUN-XVAL} ${IR-NOUN-XVAL}


#################### EVALUATING #################### 
PREPROCESSED = $(LANGS:%=${TMP}preprocess-%)
runeval: preprocess  ${EVAL-RES}
	echo "evaluation done"

${EVAL-RES}:
	mkdir -p ${OUTPUT}
	${XEVAL} -xval ${INDEXFILE} -config ${CONFFILE} -c ${CLASSIFIERS} -l ${LANGS}  -o $@

xeval: ${EVAL-RES}
	echo "printed results to" ${EVAL-RES}

# For evaluating only one language: make output/en_nouns.txt
${OUTPUT}%.txt: ${TMP}preprocess-%
	mkdir -p ${OUTPUT}
	${XEVAL} -xval ${INDEXFILE} -config ${CONFFILE} -c ${CLASSIFIERS} -l $*  -o $@



#################### PREPROCESSING #################### 

.SECONDEXPANSION:

${TMP}preprocess-%: $$(%)
	mkdir -p ${TMP}
	${PREPROCESS} ${SEED} ${FOLDS} ${NO_BASEFORMS} ${INDEXFILE} $+ ${CONFFILE} ${TMP} 2> $@.err && \
	echo "ok" > $@

preprocess: ${TMP} ${CONFFILE} ${INDEXFILE} ${PREPROCESSED}
	echo "ALL DONE"
	touch preprocess

clean:
	rm -fr ${TMP}

cleanall:
	rm -f ${CONFFILE}
	touch ${CONFFILE}
	rm -fr ${INDEXDIR}/*
	mkdir -p ${INDEXDIR}
	touch ${INDEXFILE}
	rm -fr preprocess
	rm -fr ${TMP}
	rm -f ${OUTPUT}/*



${INDEXFILE}: ${INDEXDIR}
	touch ${INDEXFILE}
${INDEXDIR}:
	mkdir -p ${INDEXDIR}
${CONFFILE}:
	touch ${CONFFILE}
${TMP}:
	mkdir -p ${TMP}



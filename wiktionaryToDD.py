
# coding: utf-8

# In[1]:

# Execute code:
# python wiktionaryToDD-v2.py "/export/a14/jsglassman/WiktionaryDataWithMappings/originalFromChris/" "/export/a14/jsglassman/WiktionaryDataWithMappings/ddRd2/"
# Converts data automatically extracted from Wiktionary and mapped to a universal feature schema into a form
# that can be used by Durrett and DeNero's program for learning paradigms.
import pandas as pd
import os
#import numpy as np
import csv
import re
import sys


# In[ ]:

def wiktionaryToDD(wiktionaryDataDirectory,ddFormattedDirectory):    # Directory must end in "/". Requires the file "universal_features.xls" to be in the same directory.
#    uLabels = pd.io.parsers.read_csv(wiktionaryDataDirectory+"universal_features.csv",dtype=unicode,encoding="utf-8") # needed the sheetname argument to run for JCS...
    # Just need the categorySet that Chris used when he did the feature mapping
#    categorySet = set(uLabels.CATEGORY)
    categorySet = set(["Polarity","Information Structure","Mood","Part of Speech","Definiteness","Switch-Reference","Politeness","Possession","Interrogativity","Aktionsart","Tense","Case","Comparison","Valency","Gender","Person","Voice","Animacy","Number","Finiteness","Aspect","Evidentiality","Deixis"])
    wiktionaryDataDirectoryFileList = os.listdir(wiktionaryDataDirectory)    # Creates a list of the files in that directory.
    for filename in wiktionaryDataDirectoryFileList:                # Loop through all the files that have Wiktionary inflected forms mapped to features in the universal schema
        if filename.endswith("_table.csv"):                         # Find non-conflicts files with data:
            language = unicode(filename.rsplit("_",1)[0],"utf-8")   # Discover the language name from the file name and put it in unicode.
            sys.stderr.write(language.encode('UTF-8'))              # JUST FOR VISUALIZATION DURING PROCESSING
            dataPath = wiktionaryDataDirectory+filename             # Create a full path to the data file.
            wD = pd.io.parsers.read_csv(dataPath,dtype=unicode,encoding="utf-8")  # Load each CSV data file as a pandas data frame with data encoded as UTF-8.
            DDwD_object = open(ddFormattedDirectory+language.encode("UTF-8")+"_DD.csv","wb")    # DDwD = {D}urrett & {D}eNero-style {w}iktionary {D}ata  --  Create a new CSV file for writing in that has the name of the language and the suffix _DD
            DDwD = csv.writer(DDwD_object)                          # Create a writable object for the CSV module to write data to.
            for i in range(len(wD.page_url)):                       # For every inflected form in the data file,
                if pd.isnull(wD.cell_value[i]) == False:
                    infl = wD.cell_value[i]                         # get the inflected form from the cell_value column at the current index and make sure it's in UTF-8
                if pd.isnull(wD.page_url[i]) == False:
                    lemma = wD.page_url[i]                          # get the lemma name from the page_url column at the current index and make sure it's in UTF-8
                origFeatureVector = ""                                  # initialize a string that will hold the feature vector.
                for dimension in categorySet:                       # then, for each column that has the name of a dimension of meaning encoded by morphology,
                    if pd.isnull(wD[dimension][i]) == True:
                        pass
                    else:
                        feature = wD[dimension][i]                  # store the value of that column in the current row as rawFeature
                        if "{" in feature or "}" in feature:
                            feature = re.sub('[\{\}]','',feature)
#                            feature = feature.strip("{}")
                            splitFeatures = feature.split(",")
                            newFeature = ""
                            for j in splitFeatures:
                                if newFeature == "":
                                    newFeature = j
                                else:
                                    newFeature = newFeature+"+"+j
                            if newFeature == "":
                                pass
                            else:
                                feature = newFeature
                        dim = dimension.lower().replace(" ","_")    # convert the dimension name to lower case with no spaces
                        if origFeatureVector == "":                     # if this is the first feature for this inflected form,
                            origFeatureVector = dim+"="+feature      # make the feature vector just this category=this feature
                        else:                                       # otherwise
                            origFeatureVector = origFeatureVector+":"+dim+"="+feature  # separate this cat=feature specification from the previous material with the colon as a delimiter, matching D & DN's CSV format
                origFeatureVector = origFeatureVector.split(":")
                origFeatureVector.sort()
                featureVector = origFeatureVector[0]
                if len(origFeatureVector) > 1:
                    for k in origFeatureVector[1:]:
                        featureVector = featureVector+":"+k
                newrow = [infl.encode("utf-8"),lemma.encode("utf-8"),featureVector.encode("utf-8")]                 # Create a list that represents the data that we want to write to the CSV. This is structured to conform to Durrett & DeNero's CSV format
                DDwD.writerow(newrow)                               # Write the data in Durrett and DeNero's format to the CSV named Language_DD.csv
                if i % 1000 == 0:
                    sys.stderr.write(".")                           # JUST FOR VISUALIZATION DURING PROCESSING
            DDwD_object.close()

# In[ ]:

wiktionaryDataDirectory = sys.argv[1]  # Where the Wiktionary data files are. This must end with "/".
ddFormattedDirectory = sys.argv[2]     # Where the produced files should be placed. This must end with "/".

wiktionaryToDD(wiktionaryDataDirectory,ddFormattedDirectory)


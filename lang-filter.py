
# coding: utf-8

# In[1]:

import pandas as pd
import os
import csv
import sys


# In[ ]:

def filterLgSpecData(wiktionaryDataFilesPath,wiktionaryDataFileName,outputPath):
    languagePos = wiktionaryDataFileName.rsplit(".",1)[0]
    if languagePos in ["inflections_input_Bulgarian_verb","inflections_input_French_verb",
    "inflections_input_German_noun","inflections_input_Italian_verb","inflections_input_Romanian_noun","inflections_input_Spanish_verb"]:
        wD = pd.io.parsers.read_csv(wiktionaryDataFilesPath+wiktionaryDataFileName,header=None,dtype=unicode,encoding="utf-8",names=["infl","lemma","featureVector"])
        filteredData = open(outputPath+wiktionaryDataFileName,"wb")
        filtered = csv.writer(filteredData)
    else:
        pass
#    languageName = wiktionaryDataFileName.rsplit("_",2)[0]
#    languageName = languageName.rsplit("/",1)[1]
    if languagePos == "inflections_input_Bulgarian_verb":
        for i in range(len(wD.lemma)):
            if wD.featureVector[i] == "pos=V":
                pass
            else:
                newrow = [wD.infl[i].encode("utf-8"),wD.lemma[i].encode("utf-8"),wD.featureVector[i].encode("utf-8")]
                filtered.writerow(newrow)
    elif languagePos == "inflections_input_French_verb":
        for i in range(len(wD.lemma)):
            if wD.featureVector[i] == "gender=FEM:number=PL:pos=V" or wD.featureVector[i] == "gender=FEM:pos=V" or wD.featureVector[i] == "gender=MASC:number=PL:pos=V":
                pass
            else:
                newrow = [wD.infl[i].encode("utf-8"),wD.lemma[i].encode("utf-8"),wD.featureVector[i].encode("utf-8")]
                filtered.writerow(newrow)
    elif languagePos == "inflections_input_German_noun":
        germanArticles = ["ein","eine","einen","einem","einer","eines","der","das","die","den","dem","des"]
        for i in range(len(wD.lemma)):
            if wD.infl[i] in germanArticles or wD.featureVector[i] == "case=GEN:pos=N" or wD.featureVector[i] == "number=PL:pos=N":
                pass
            else:
                newrow = [wD.infl[i].encode("utf-8"),wD.lemma[i].encode("utf-8"),wD.featureVector[i].encode("utf-8")]
                filtered.writerow(newrow)
    elif languagePos == "inflections_input_Italian_verb":
        for i in range(len(wD.lemma)):
            if wD.featureVector[i] == "gender=FEM:number=PL:pos=V" or wD.featureVector[i] == "gender=FEM:pos=V" or wD.featureVector[i] == "gender=MASC:number=PL:pos=V":
                pass
            else:
                newrow = [wD.infl[i].encode("utf-8"),wD.lemma[i].encode("utf-8"),wD.featureVector[i].encode("utf-8")]
                filtered.writerow(newrow)
    elif languagePos == "inflections_input_Romanian_noun":
        for i in range(len(wD.lemma)):
            if wD.featureVector[i] == "number=PL:pos=N":
                pass
            else:
                newrow = [wD.infl[i].encode("utf-8"),wD.lemma[i].encode("utf-8"),wD.featureVector[i].encode("utf-8")]
                filtered.writerow(newrow)
    elif languagePos == "inflections_input_Spanish_verb":
        for i in range(len(wD.lemma)):
            if wD.lemma[i].endswith("ar") or wD.lemma[i].endswith("er") or wD.lemma[i].endswith("ir"):   
                newrow = [wD.infl[i].encode("utf-8"),wD.lemma[i].encode("utf-8"),wD.featureVector[i].encode("utf-8")]
                filtered.writerow(newrow)


# In[ ]:

wiktionaryDataFilesPath = sys.argv[1]
outputPath = sys.argv[2]
wiktionaryDataFilesList = os.listdir(wiktionaryDataFilesPath)
for i in wiktionaryDataFilesList:
    if i.endswith("_noun.csv") or i.endswith("_verb.csv"):
        filterLgSpecData(wiktionaryDataFilesPath,i,outputPath)


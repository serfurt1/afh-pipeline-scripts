"""
This program extracts the maximal paradigm from the inflections data, and the corresponding lemmas.
It then creates a new inflection data file, containing the data for the extracted lemmas, with the 
data written in a specific sequence of the feature vectors. It removes duplicates by taking the 
first form that appears.
This is necessary because the AFH program expects the data in this form(essentially like 
a real table.) 
"""

import pandas as pd
import os
import sys
import csv
from collections import defaultdict
import codecs


def buildInflectionsDataForMaximalParadigms(goldParadigmsDir,inflectionDataDir,newInflectionDataDir):
    goldParadigmsFileList = os.listdir(goldParadigmsDir)
    inflectionDataFileList = os.listdir(inflectionDataDir)

    for goldParadigmsFile in goldParadigmsFileList:
        if goldParadigmsFile.endswith("_stdout.txt"):
            goldParadigmsFileObj = open(goldParadigmsDir+goldParadigmsFile,'r')
            lines = goldParadigmsFileObj.readlines()
            if "Attribute set counts\n" in lines:
                maxParadigmLine = lines.index("Attribute set counts\n")+1
                if len(lines[maxParadigmLine].split(": ",1)) > 1:
                    maxVector = lines[maxParadigmLine].split(": ",1)[1]
                    maxVector = maxVector.rstrip("\n")
                    maxVectorFeatureList = maxVector.strip("[]").split(", ")
                    maxVectorFeatureSet = set(maxVectorFeatureList)
                    language_pos = goldParadigmsFile.rsplit("_",1)[0]
                    inflectionData = pd.read_csv(inflectionDataDir+language_pos+".csv",header=None,dtype=unicode,encoding="utf-8",names=["infl","lemma","featureVector"])
                    inflectionDataLemmas = defaultdict(set)
                    lemma_featureVec_infl = defaultdict(dict)
                    #lemma_baseform_feat_vec = {}
                    for i in range(len(inflectionData.lemma)):
                        inflectionDataLemmas[inflectionData.lemma[i]].add(inflectionData.featureVector[i])
                        if not lemma_featureVec_infl[inflectionData.lemma[i]]:  # if no inflection data for this lemma yet, create new dictionary
                            lemma_featureVec_infl[inflectionData.lemma[i]] = {}
                        if inflectionData.featureVector[i] not in lemma_featureVec_infl[inflectionData.lemma[i]].keys(): # add if there isn't already an inflected forms for this feature vector
                            lemma_featureVec_infl[inflectionData.lemma[i]][inflectionData.featureVector[i]] = inflectionData.infl[i]
                        #if inflectionData.lemma[i] not in lemma_baseform_feat_vec.keys():
                        #    lemma_baseform_feat_vec[inflectionData.lemma[i]] = inflectionData.featureVector[i] 
                    goldTestLemmas = set()
                    for j in inflectionDataLemmas:
                        if len(inflectionDataLemmas[j] ^ maxVectorFeatureSet) == 0:
                            goldTestLemmas.add(j)
                    new_inflections_lines = []
                    for lemma in goldTestLemmas:
                        # Works better for AFH if the first form for the lemma is itself - Input
                        new_inflections_lines.append('%s,%s,%s' % (lemma, lemma, 'Input'))
                        for featureVec in maxVectorFeatureSet: # - set([lemma_baseform_feat_vec[lemma]]):
                            new_inflections_lines.append('%s,%s,%s' % (lemma_featureVec_infl[lemma][featureVec], lemma, featureVec))
                    codecs.open('%s%s.csv' %(newInflectionDataDir,language_pos), 'w', encoding='utf-8').write('\n'.join(new_inflections_lines))
                    

# In[ ]:
if len(sys.argv) != 4:
    print 'Usage: pipeline-extract-max-paradigm-and-inflections [Durrett_DeNero_output_directory inflections_data_directory new_inflections_data_directory]'
    sys.exit()

goldParadigmsDir = sys.argv[1] # the directory containing the output from Durrett and DeNero from which the maximal paradigms can be gotten
inflectionDataDir = sys.argv[2] # the directory containing the inflections data
newInflectionDataDir = sys.argv[3] # the directory where the new inflections data will be written to

buildInflectionsDataForMaximalParadigms(goldParadigmsDir,inflectionDataDir,newInflectionDataDir)


AFH Implementation notes:
@SEAN
    Prereqs: Python 2.7 and Perl 5 (should already be on grid)

    1. Download AFH code/data from https://svn.spraakbanken.gu.se/clt/naacl/2015/extract ,
	maintaining the exact same directory structure.

    2. Download and install the foma toolkit from 
    https://code.google.com/p/foma/downloads/list . and add it to your PATH.
	Notes:
		To compile foma (and flookup) as well as the foma static and dynamic library, 
		"$ make; make install" should work on most UNIX systems.  The default 
		installation target /usr/local can be changed in the first line of the Makefile

		Add local binaries to PATH:
		$ export PATH=$PATH:~/bin

    3. Also requires numpy, scipy, and scikit-learn (can be installed with pip in a virtual environment)
	Notes on virtualenv:
		This is necessary because the version of scikit-learn used in the code is more recent than the 
		version found on the grid. So, to compile on your own system, you can just install the latest
		scikit-learn and skip this step.

		Download
		$ curl -s https://pypi.python.org/packages/source/v/virtualenv/virtualenv-13.0.3.tar.gz | tar zx

		Navigate to directory
		$ cd virtualenv-13.0.3

		Run
		$ python virtualenv.py ~/

		Start up environment (once per session)
		$ . ~/bin/activate

		Use pip as normal to install required python modules.
		$ pip install numpy scipy scikit-learn

	4. Update the AFH code files, with the updated versions in this repo.
		See (afh-code-changes) for details of the files that were changed and the details of the 
		changes.		

	5. Get the data into the required format.
		
		The first 4 steps below are also required to run the Durrett and DeNero system. If that has been done previously,
		then use the files from that and skip to the 5th step. Check ‘assignment1-implement_durrett_denero_2013.txt’ for more
		details on running the Durrett and DeNero system. The scripts were gotten from the ’/export/a14/jsglassman/’ 
		directory on the CLSP grid

		- Convert the wiktionary data into the Durrett and DeNero format using John’s 
		‘pipeline-0-wiktionaryToDD.py’ script. This should output files in the format Language_DD.csv.

		- Run ‘pipeline-1-generate-n-v-per-language.py’ to separate the data for each language into nouns and verbs.
		- Get the maximal paradigms by running the Durrett & DeNero software on the output gotten above.
		- Run ‘pipeline-4-extract-max-paradigm-build-train-test.py’ to get training and test splits for the lemmas. The test
		test data is used for prediction, to compare predicted inflected forms with output from other systems.

		- Copy the train and test lemma files to the directory for the input files, usually a sub-directory of ‘data/’
		 in the AFH structure. 

		- Run ‘extract-max-paradigm-and-rewrite-inflections-data.py’ to rewrite the inflections data in the format compatible
		with the code. It extracts only the tables for the lemmas with the maximal paradigms, and ensures that the data
		is in the same order of the feature vectors for all the lemmas. The output directory(last argument) should be set to the 
		input files directory containing the test and train files from the previous step.

	6. To run for a single language and POS, (start virtualenv and) run:  
		$ ./run_per_language.sh (LANGUAGE)_(POS)

	7. To make predictions of inflected forms with a test file of lemmas, run:
		python src/predict.py -test (TEST_LEMMAS_FILE) -infl (INFLECTIONS_INPUT_FILE) -xval data/xval/index.txt 
		-config classifier_svm.conf -C classifier_svm -l (LANGUAGE)_(POS)  -o (PREDICTIONS_OUTPUT_DIR)



Aileme notes:
	This should have been fine, but I discovered that their SVM code crashes 
	if there are just two classes in the training data, meaning there are just two 
	paradigms to choose from, so I made changes to classifier_svm.py for that.
	When there is only one paradigm rule(like for Quechua verbs, seems all the verbs 
	have the same inflection rules), the code still crashes. Obviously there is no 
	need to run an SVM in this case since there is no classification to be done. 
	Still not sure how to handle this case.

Sean notes: 
	Bug:
		code gives ZeroDivisionError in evaluate.eval() when scoring generated inflections 
	Fix:
		changed line 180 in evaluate.py, starts count at 1, not 0. New code may give 
		inaccurate scoring but at least doesn't fail.

	Bug:
		couldnt read some xval files because it assumed one-word only language names
	Fix:
		Edited line 24 in preprocess.py to allow for multi-word language names.

	Bug:
		Error code: make: *** [tmp/preprocess-Faliscan_verb] Error 1
		happens when there is insufficient data to make train and 
		test samples

	Bug: 
		data/xval/index.txt duplicates languages it has already seen
		even if no further processing happens, leading to a massive
		file that will be slow to scan in later runs.
	Fix:
		delete index.txt between runs, or remove the language files
		you have already ran AFH on.

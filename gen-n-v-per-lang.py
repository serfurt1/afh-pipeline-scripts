
# coding: utf-8

# In[13]:

import pandas as pd
import codecs
import os
import csv
import sys
import re


# In[3]:

def generateNVLanguageData(wiktionaryDataDir,outputDir,make):    # input should be of the form (with quotes) "Language Name_DD.csv" - There are spaces in the language names, but _DD.csv is always tacked on. make=True only creates makefile
	wiktionaryDataFileList = os.listdir(wiktionaryDataDir)
	# create Makefile
	config = open("Makefile","wb")
	config.write("""
# PROGRAMS
P=python src/paradigms.py
PP=python printwordforms.py
E=./extract.perl -u
XEVAL=python src/evaluate.py
PREPROCESS=python src/preprocess.py
SEED=41
FOLDS=5
NO_BASEFORMS=5000
CLASSIFIERS=classifier_dummy classifier_most_freq classifier_maxsuff classifier_svm


CONFFILE=classifier_svm.conf
INDEXDIR=data/xval/
INDEXFILE=${INDEXDIR}index.txt
OUTPUT=output/
EVAL-RES=${OUTPUT}/eval.txt
TMP=tmp/

include Makefile.data

############ FILE LISTS FOR CROSS VALIDIATION #############
# All languages should be declared here
LANGS= """)
	langs = []
	temp = []
	# create Makefile.data
	data = open("Makefile.data","wb")
	data.write("##################### DATA #################### \n" +
			   "# WIKIPEDIA STATISTICS \n" +
			   "WIKIEN=data/wikipedia/stats_en.txt \n" +
			   "\n# INFLECTIONS\n")
	for wiktionaryDataFile in wiktionaryDataFileList:
		if wiktionaryDataFile.endswith("_DD.csv"):
			languageName = wiktionaryDataFile.rsplit("_",1)[0].replace(" ", "_")
			if not make:
				wiktionaryDataPath = wiktionaryDataDir+wiktionaryDataFile
				wD = pd.read_csv(wiktionaryDataPath,header=None,dtype=unicode,encoding="utf-8",names=["infl","lemma","featureVector"])
				# Sean: modified for AFH expected filename format
				nounsData = open(outputDir+"inflections_input_"+languageName+"_noun.csv","wb")
				nouns = csv.writer(nounsData)
				verbsData = open(outputDir+"inflections_input_"+languageName+"_verb.csv","wb")
				verbs = csv.writer(verbsData)
				nounLemmaIndices = set()
				verbLemmaIndices = set()
				excludeIfInflEquals = [u"singular",u"Singular",u"plural",u"Plural",u"none",u"Present",u"Past",u"Future",u"Masculine",u"Feminine",u"Neuter",u"animate",u"inanimate",u"acc.",u"first",u"second",u"third",u"all",u"eg",u"sg",u"pl",u"Present",u"Imperfect",u"Dependent",u"Perfective",u"Perfect",u"Continuous",u"Simple",u"Imperfective",u"Nominative",u"Vocative",u"Genitive",u"Dative",u"Passive",u"Causative",u"Potential",u"Volitional",u"Negative",u"Formal",u"Conjunctive",u"Determiners",u"imperative",u"unchanged",u"*rare",u"Oblique",u"Subject",u"1st",u"2nd",u"3rd",u"Aorist",u"Imperative",u"Pluperfect",u"Number",u"Person",u"—3",u"*"]
				excludeIfInInfl = [u" ",u",",u"-",u"—",u":",u"(",u")",u"[",u"]",u"{",u"}"]
				splitIfInInfl = ["/",";"]
				for i in range(len(wD.lemma)):
					if pd.isnull(wD.infl[i]) == True or pd.isnull(wD.lemma[i]) == True:
						pass
					elif wD.infl[i] in excludeIfInflEquals:
						pass
					elif any(j in wD.infl[i] for j in excludeIfInInfl):
						pass
					else: 
						if "part_of_speech=N" in wD.featureVector[i]:
							nounLemmaIndices.add(i)
						if "part_of_speech=V" in wD.featureVector[i]:
							verbLemmaIndices.add(i)
				for k in nounLemmaIndices:
					infl = wD.infl[k].strip("*")
					for l in splitIfInInfl:
						if l in wD.infl[k]:
							infl = infl.split(l)[0].strip("*")
					nounRow = [infl.encode("utf-8"),wD.lemma[k].encode("utf-8"),wD.featureVector[k].encode("utf-8")]
					nouns.writerow(nounRow)
				nounsData.close()
				for m in verbLemmaIndices:
					infl = wD.infl[m].strip("*")
					for n in splitIfInInfl:
						if n in wD.infl[m]:
							infl = infl.split(n)[0].strip("*")
					verbRow = [infl.encode("utf-8"),wD.lemma[m].encode("utf-8"),wD.featureVector[m].encode("utf-8")]
					verbs.writerow(verbRow)
				verbsData.close()
			# Sean: modified to generate lines for Makefile
			config.write(languageName+"_noun "+languageName+"_verb ")
			langs.append(languageName+"_noun")
			langs.append(languageName+"_verb")
			temp.append(languageName+"_noun= ${WIKIEN} ${"+languageName+"-INFL-NOUN} ${"+languageName+"-NOUN-XVAL}\n")
			temp.append(languageName+"_verb= ${WIKIEN} ${"+languageName+"-INFL-VERB} ${"+languageName+"-VERB-XVAL}\n")
			data.write(languageName+"-INFL-NOUN-XVAL="+outputDir+"inflections_input_"+languageName+"_noun.csv\n")
			data.write(languageName+"-INFL-VERB-XVAL="+outputDir+"inflections_input_"+languageName+"_verb.csv\n")
	data.close()
	# finish Makefile
	config.write("\n# LANGUAGES\n")
	for line in temp:
		config.write(line)

	config.write("""\n#################### EVALUATING #################### 
PREPROCESSED = $(LANGS:%=${TMP}preprocess-%)
runeval: preprocess  ${EVAL-RES}
	echo "evaluation done"

${EVAL-RES}:
	mkdir -p ${OUTPUT}
	${XEVAL} -xval ${INDEXFILE} -config ${CONFFILE} -c ${CLASSIFIERS} -l ${LANGS}  -o $@

xeval: ${EVAL-RES}
	echo "printed results to" ${EVAL-RES}

# For evaluating only one language: make output/en_nouns.txt
${OUTPUT}%.txt: ${TMP}preprocess-%
	mkdir -p ${OUTPUT}
	${XEVAL} -xval ${INDEXFILE} -config ${CONFFILE} -c ${CLASSIFIERS} -l $*  -o $@



#################### PREPROCESSING #################### 

""")
	# make individual preprocess recipes
	for l in langs:
		lang, pos = l.rsplit("_", 1)
		pos = pos.upper()
		config.write("${TMP}preprocess-"+l+":\n"+
		"\tmkdir -p ${TMP} \n"+
		"\t${PREPROCESS} ${SEED} ${FOLDS} ${NO_BASEFORMS} ${INDEXFILE} "+
		"${WIKIEN} ${"+lang+"-INFL-"+pos+"-XVAL} ${"+lang+"-"+pos+
		"-XVAL} ${CONFFILE} ${TMP} 2> $@.err && echo 'ok' > $@000 \n\n")

	config.write("""
preprocess: ${TMP} ${CONFFILE} ${INDEXFILE} ${PREPROCESSED}
	echo "ALL DONE"
	touch preprocess

clean:
	rm -fr ${TMP}

cleanall:
	rm -f ${CONFFILE}
	touch ${CONFFILE}
	rm -fr ${INDEXDIR}/*
	mkdir -p ${INDEXDIR}
	touch ${INDEXFILE}
	rm -fr preprocess
	rm -fr ${TMP}
	rm -f ${OUTPUT}/*



${INDEXFILE}: ${INDEXDIR}
	touch ${INDEXFILE}
${INDEXDIR}:
	mkdir -p ${INDEXDIR}
${CONFFILE}:
	touch ${CONFFILE}
${TMP}:
	mkdir -p ${TMP}""")
	config.close()

# In[7]:

wiktionaryDataDir = sys.argv[1]
outputDir = sys.argv[2]
#for optimization
try:
	make = sys.argv[3]
except IndexError:
	make = False
generateNVLanguageData(wiktionaryDataDir,outputDir,make)


# In[ ]:




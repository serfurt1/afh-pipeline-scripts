#!/bin/bash

#start python environment (preloaded with necessary modules as in README)
. ~/bin/activate

#create intermediate directory to hold DD-ized data
mkdir -p en_fin_dd_06_20_15

#change only 'en_finished_06_20_15/' below to point to your wiktionary data directory.
python wiktionaryToDD.py en_finished_06_20_15/ en_fin_dd_06_20_15/

#comment out previous command and add 'True' to end of next command to speed up
# if you have the inflections_input_ and base_forms_ files, but not Makefiles
python gen-n-v-per-lang.py en_fin_dd_06_20_15/ data/wiktionary-morphology-1.1/
python extract-baseforms.py -d data/wiktionary-morphology-1.1/

#run AFH for each lang,pos pair in data/wiktionary-morphology-1.1/
# run just this for-loop if all the files have been generated already
for LangPos in $(ls data/wiktionary-morphology-1.1/)
do
	#split filename up using '_'
	str=(${LangPos//_/ })
	#run once per lang,pos pair
	if [ "${str[0]}" = 'base' ]
		then
			#get length of language name in words
			len=$[ ${#str[@]} - 3 ]
			#extract language name
			lang=${str[@]:2:$len}
			#split part of speech from file extension
			pos=(${str[${#str[@]} - 1]//./ })
			#create filename
			fName="${lang[@]// /_}"_"${pos[0]}".txt
			#check if output has already been made
			# only useful if pipeline was interrupted or to add new data
			match=0
			for F in $(ls output/)
			do
				if [ "$F" = "$fName" ]
					then
						match=1
						break
				fi
			done
			#run AFH if no output found
			if [ $match -eq 0 ]
				then
					make output/"$fName"
			fi
	fi
done